# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Accessories',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=200)),
                ('mark', models.CharField(max_length=50)),
                ('status', models.CharField(max_length=2, choices=[(b'0', b'Dispon\xc3\xadvel'), (b'3', b'Reservada'), (b'2', b'Manuten\xc3\xa7\xc3\xa3o'), (b'1', b'Alugada')])),
                ('type', models.CharField(max_length=5, choices=[(b'1', b'Semi-j\xc3\xb3ia'), (b'0', b'J\xc3\xb3ia')])),
                ('model', models.CharField(max_length=5, choices=[(b'7', b'Pulseira'), (b'0', b'Anel'), (b'2', b'Brinco'), (b'3', b'Broche'), (b'6', b'Pente'), (b'5', b'Coroa'), (b'4', b'Colar'), (b'1', b'Bracelete'), (b'8', b'Tiara')])),
                ('material', models.CharField(max_length=5, choices=[(b'6', b'Ouro'), (b'1', b'Couro'), (b'0', b'A\xc3\xa7o'), (b'10', b'Strass'), (b'8', b'Prata'), (b'9', b'P\xc3\xa9rola'), (b'4', b'Folheado a prata'), (b'2', b'Cristal'), (b'5', b'Madrep\xc3\xa9rola'), (b'7', b'Ouro e prata'), (b'11', b'Outros'), (b'3', b'Folheado a ouro')])),
            ],
            options={
                'verbose_name': 'Acess\xf3rio',
                'verbose_name_plural': 'Acess\xf3rios',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Bag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=200)),
                ('mark', models.CharField(max_length=50)),
                ('status', models.CharField(max_length=2, choices=[(b'0', b'Dispon\xc3\xadvel'), (b'3', b'Reservada'), (b'2', b'Manuten\xc3\xa7\xc3\xa3o'), (b'1', b'Alugada')])),
                ('gender', models.CharField(max_length=5, choices=[(b'1', b'Feminino'), (b'0', b'Masculino')])),
                ('model', models.CharField(max_length=5, choices=[(b'1', b'Carteira/Cluth'), (b'2', b'Carteiro'), (b'0', b'Ba\xc3\xba'), (b'4', b'Transversal'), (b'6', b'Outros'), (b'5', b'Carteira'), (b'3', b'Sacola')])),
            ],
            options={
                'verbose_name': 'Bolsa',
                'verbose_name_plural': 'Bolsas',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Blazer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=200)),
                ('mark', models.CharField(max_length=50)),
                ('status', models.CharField(max_length=2, choices=[(b'0', b'Dispon\xc3\xadvel'), (b'3', b'Reservada'), (b'2', b'Manuten\xc3\xa7\xc3\xa3o'), (b'1', b'Alugada')])),
                ('size', models.CharField(max_length=5, choices=[(b'EG', b'EG'), (b'GG', b'GG'), (b'34', b'34'), (b'50', b'50'), (b'G', b'G'), (b'44', b'44'), (b'48', b'48'), (b'EGG', b'EGG'), (b'36', b'36'), (b'54', b'54'), (b'52', b'52'), (b'46', b'46'), (b'40', b'40'), (b'PP', b'PP'), (b'42', b'42'), (b'M', b'M'), (b'P', b'P'), (b'38', b'38')])),
            ],
            options={
                'verbose_name': 'Blazer',
                'verbose_name_plural': 'Blazers',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Color',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to=b'uploads/colors')),
            ],
            options={
                'verbose_name': 'Cor',
                'verbose_name_plural': 'Cores',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Dress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=200)),
                ('mark', models.CharField(max_length=50)),
                ('status', models.CharField(max_length=2, choices=[(b'0', b'Dispon\xc3\xadvel'), (b'3', b'Reservada'), (b'2', b'Manuten\xc3\xa7\xc3\xa3o'), (b'1', b'Alugada')])),
                ('type', models.CharField(max_length=2, choices=[(b'2', b'Outro'), (b'1', b'Noiva'), (b'0', b'Festa')])),
                ('model', models.CharField(max_length=2, choices=[(b'2', b'Outro'), (b'1', b'Curto'), (b'0', b'Longo')])),
                ('size', models.CharField(max_length=5, choices=[(b'EG', b'EG'), (b'GG', b'GG'), (b'34', b'34'), (b'50', b'50'), (b'G', b'G'), (b'44', b'44'), (b'48', b'48'), (b'EGG', b'EGG'), (b'36', b'36'), (b'54', b'54'), (b'52', b'52'), (b'46', b'46'), (b'40', b'40'), (b'PP', b'PP'), (b'42', b'42'), (b'M', b'M'), (b'P', b'P'), (b'38', b'38')])),
                ('colors', models.ManyToManyField(to='parts.Color', null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Vestido',
                'verbose_name_plural': 'Vestidos',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FormalPants',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=200)),
                ('mark', models.CharField(max_length=50)),
                ('status', models.CharField(max_length=2, choices=[(b'0', b'Dispon\xc3\xadvel'), (b'3', b'Reservada'), (b'2', b'Manuten\xc3\xa7\xc3\xa3o'), (b'1', b'Alugada')])),
                ('size', models.CharField(max_length=5, choices=[(b'60', b'60'), (b'50', b'50'), (b'42', b'42'), (b'47', b'47'), (b'61', b'61'), (b'45', b'45'), (b'40', b'40'), (b'46', b'46'), (b'37', b'37'), (b'51', b'51'), (b'34', b'34'), (b'48', b'48'), (b'53', b'53'), (b'49', b'49'), (b'35', b'35'), (b'62', b'62'), (b'43', b'43'), (b'39', b'39'), (b'38', b'38'), (b'58', b'58'), (b'57', b'57'), (b'44', b'44'), (b'36', b'36'), (b'54', b'54'), (b'41', b'41'), (b'56', b'56'), (b'55', b'55'), (b'59', b'59'), (b'52', b'52')])),
                ('colors', models.ManyToManyField(to='parts.Color', null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Cal\xe7a Social',
                'verbose_name_plural': 'Cal\xe7as Social',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FormalShirt',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=200)),
                ('mark', models.CharField(max_length=50)),
                ('status', models.CharField(max_length=2, choices=[(b'0', b'Dispon\xc3\xadvel'), (b'3', b'Reservada'), (b'2', b'Manuten\xc3\xa7\xc3\xa3o'), (b'1', b'Alugada')])),
                ('arm', models.CharField(max_length=5, choices=[(b'PP', b'PP'), (b'GG', b'GG'), (b'M', b'M'), (b'P', b'P'), (b'G', b'G')])),
                ('size', models.CharField(max_length=5, choices=[(b'EG', b'EG'), (b'GG', b'GG'), (b'34', b'34'), (b'50', b'50'), (b'G', b'G'), (b'44', b'44'), (b'48', b'48'), (b'EGG', b'EGG'), (b'36', b'36'), (b'54', b'54'), (b'52', b'52'), (b'46', b'46'), (b'40', b'40'), (b'PP', b'PP'), (b'42', b'42'), (b'M', b'M'), (b'P', b'P'), (b'38', b'38')])),
                ('colors', models.ManyToManyField(to='parts.Color', null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Camisa Social',
                'verbose_name_plural': 'Camisas Social',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, null=True, blank=True)),
                ('image', models.ImageField(upload_to=b'uploads/photos')),
            ],
            options={
                'verbose_name': 'Photo',
                'verbose_name_plural': 'Photos',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Shoe',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=200)),
                ('mark', models.CharField(max_length=50)),
                ('status', models.CharField(max_length=2, choices=[(b'0', b'Dispon\xc3\xadvel'), (b'3', b'Reservada'), (b'2', b'Manuten\xc3\xa7\xc3\xa3o'), (b'1', b'Alugada')])),
                ('gender', models.CharField(max_length=5, choices=[(b'1', b'Feminino'), (b'0', b'Masculino')])),
                ('model', models.CharField(max_length=5, choices=[(b'8', b'Sapat\xc3\xaanis'), (b'1', b'Mocassim'), (b'10', b'Tamanco'), (b'3', b'Open boot'), (b'12', b'Sapato social'), (b'0', b'Melissa'), (b'11', b'Sapatilha'), (b'4', b'Oxford'), (b'9', b'Scarpins'), (b'13', b'Outros'), (b'2', b'Mules'), (b'6', b'Plataforma'), (b'5', b'Peep Toe'), (b'7', b'Sand\xc3\xa1lias')])),
                ('size', models.CharField(max_length=5, choices=[(b'37', b'37'), (b'42', b'42'), (b'50', b'50'), (b'48', b'48'), (b'44', b'44'), (b'51', b'51'), (b'36', b'36'), (b'47', b'47'), (b'49', b'49'), (b'35', b'35'), (b'46', b'46'), (b'43', b'43'), (b'38', b'38'), (b'40', b'40'), (b'39', b'39'), (b'41', b'41'), (b'45', b'45')])),
                ('colors', models.ManyToManyField(to='parts.Color', null=True, blank=True)),
                ('images', models.ManyToManyField(to='parts.Photo', null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Sapato',
                'verbose_name_plural': 'Sapatos',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Suit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=200)),
                ('mark', models.CharField(max_length=50)),
                ('status', models.CharField(max_length=2, choices=[(b'0', b'Dispon\xc3\xadvel'), (b'3', b'Reservada'), (b'2', b'Manuten\xc3\xa7\xc3\xa3o'), (b'1', b'Alugada')])),
                ('size', models.CharField(max_length=5, choices=[(b'EG', b'EG'), (b'GG', b'GG'), (b'34', b'34'), (b'50', b'50'), (b'G', b'G'), (b'44', b'44'), (b'48', b'48'), (b'EGG', b'EGG'), (b'36', b'36'), (b'54', b'54'), (b'52', b'52'), (b'46', b'46'), (b'40', b'40'), (b'PP', b'PP'), (b'42', b'42'), (b'M', b'M'), (b'P', b'P'), (b'38', b'38')])),
                ('colors', models.ManyToManyField(to='parts.Color', null=True, blank=True)),
                ('images', models.ManyToManyField(to='parts.Photo', null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Palet\xf3',
                'verbose_name_plural': 'Palet\xf3s',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Texture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to=b'uploads/textures')),
            ],
            options={
                'verbose_name': 'Textura',
                'verbose_name_plural': 'Texturas',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=200)),
                ('mark', models.CharField(max_length=50)),
                ('status', models.CharField(max_length=2, choices=[(b'0', b'Dispon\xc3\xadvel'), (b'3', b'Reservada'), (b'2', b'Manuten\xc3\xa7\xc3\xa3o'), (b'1', b'Alugada')])),
                ('size', models.CharField(max_length=5, choices=[(b'PP', b'PP'), (b'GG', b'GG'), (b'M', b'M'), (b'P', b'P'), (b'G', b'G')])),
                ('colors', models.ManyToManyField(to='parts.Color', null=True, blank=True)),
                ('images', models.ManyToManyField(to='parts.Photo', null=True, blank=True)),
                ('textures', models.ManyToManyField(to='parts.Texture', null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Gravata',
                'verbose_name_plural': 'Gravatas',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='suit',
            name='textures',
            field=models.ManyToManyField(to='parts.Texture', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='shoe',
            name='textures',
            field=models.ManyToManyField(to='parts.Texture', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='formalshirt',
            name='images',
            field=models.ManyToManyField(to='parts.Photo', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='formalshirt',
            name='textures',
            field=models.ManyToManyField(to='parts.Texture', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='formalpants',
            name='images',
            field=models.ManyToManyField(to='parts.Photo', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='formalpants',
            name='textures',
            field=models.ManyToManyField(to='parts.Texture', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dress',
            name='images',
            field=models.ManyToManyField(to='parts.Photo', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dress',
            name='textures',
            field=models.ManyToManyField(to='parts.Texture', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blazer',
            name='colors',
            field=models.ManyToManyField(to='parts.Color', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blazer',
            name='images',
            field=models.ManyToManyField(to='parts.Photo', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blazer',
            name='textures',
            field=models.ManyToManyField(to='parts.Texture', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bag',
            name='colors',
            field=models.ManyToManyField(to='parts.Color', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bag',
            name='images',
            field=models.ManyToManyField(to='parts.Photo', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bag',
            name='textures',
            field=models.ManyToManyField(to='parts.Texture', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='accessories',
            name='colors',
            field=models.ManyToManyField(to='parts.Color', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='accessories',
            name='images',
            field=models.ManyToManyField(to='parts.Photo', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='accessories',
            name='textures',
            field=models.ManyToManyField(to='parts.Texture', null=True, blank=True),
            preserve_default=True,
        ),
    ]
