from django.contrib import admin
from apps.parts.models import Color, Texture, Photo, Dress, Blazer, Suit, Tie, FormalShirt, FormalPants, Shoe, \
    Accessories, Bag

admin.site.register(Color)
admin.site.register(Texture)
admin.site.register(Photo)
admin.site.register(Dress)
admin.site.register(Blazer)
admin.site.register(Suit)
admin.site.register(Tie)
admin.site.register(FormalShirt)
admin.site.register(FormalPants)
admin.site.register(Accessories)
admin.site.register(Shoe)
admin.site.register(Bag)
