# -*- coding: utf-8 -*-
from django.db import models

PART_STATUS = {
    ('0', 'Disponível'),
    ('1', 'Alugada'),
    ('2', 'Manutenção'),
    ('3', 'Reservada')
}

DRESS_TYPE = {
    ('0', 'Festa'),
    ('1', 'Noiva'),
    ('2', 'Outro')
}

DRESS_MODEL = {
    ('0', 'Longo'),
    ('1', 'Curto'),
    ('2', 'Outro')
}

SIZE = {
    ('34', '34'),
    ('36', '36'),
    ('38', '38'),
    ('40', '40'),
    ('42', '42'),
    ('44', '44'),
    ('46', '46'),
    ('48', '48'),
    ('50', '50'),
    ('52', '52'),
    ('54', '54'),
    ('PP', 'PP'),
    ('P', 'P'),
    ('M', 'M'),
    ('G', 'G'),
    ('GG', 'GG'),
    ('EG', 'EG'),
    ('EGG', 'EGG')
}

TIE_SIZE = {
    ('PP', 'PP'),
    ('P', 'P'),
    ('M', 'M'),
    ('G', 'G'),
    ('GG', 'GG')
}

PANTS_SIZE = {
    ('34', '34'),
    ('35', '35'),
    ('36', '36'),
    ('37', '37'),
    ('38', '38'),
    ('39', '39'),
    ('40', '40'),
    ('41', '41'),
    ('42', '42'),
    ('43', '43'),
    ('44', '44'),
    ('45', '45'),
    ('46', '46'),
    ('47', '47'),
    ('48', '48'),
    ('49', '49'),
    ('50', '50'),
    ('51', '51'),
    ('52', '52'),
    ('53', '53'),
    ('54', '54'),
    ('55', '55'),
    ('56', '56'),
    ('57', '57'),
    ('58', '58'),
    ('59', '59'),
    ('60', '60'),
    ('61', '61'),
    ('62', '62')
}

ARM_TYPE = {
    ('0', 'Curta'),
    ('1', 'Longa'),
    ('2', '3x4')
}

ACCESSORIES_TYPE = {
    ('0', 'Jóia'),
    ('1', 'Semi-jóia')
}

ACCESSORIES_MODEL = {
    ('0', 'Anel'),
    ('1', 'Bracelete'),
    ('2', 'Brinco'),
    ('3', 'Broche'),
    ('4', 'Colar'),
    ('5', 'Coroa'),
    ('6', 'Pente'),
    ('7', 'Pulseira'),
    ('8', 'Tiara')
}

ACCESSORIES_MATERIAL = {
    ('0', 'Aço'),
    ('1', 'Couro'),
    ('2', 'Cristal'),
    ('3', 'Folheado a ouro'),
    ('4', 'Folheado a prata'),
    ('5', 'Madrepérola'),
    ('6', 'Ouro'),
    ('7', 'Ouro e prata'),
    ('8', 'Prata'),
    ('9', 'Pérola'),
    ('10', 'Strass'),
    ('11', 'Outros')
}

GENDER = {
    ('0', 'Masculino'),
    ('1', 'Feminino')
}

SHOES_MODEL = {
    ('0', 'Melissa'),
    ('1', 'Mocassim'),
    ('2', 'Mules'),
    ('3', 'Open boot'),
    ('4', 'Oxford'),
    ('5', 'Peep Toe'),
    ('6', 'Plataforma'),
    ('7', 'Sandálias'),
    ('8', 'Sapatênis'),
    ('9', 'Scarpins'),
    ('10', 'Tamanco'),
    ('11', 'Sapatilha'),
    ('12', 'Sapato social'),
    ('13', 'Outros')
}

SHOES_SIZE = {
    ('35', '35'),
    ('36', '36'),
    ('37', '37'),
    ('38', '38'),
    ('39', '39'),
    ('40', '40'),
    ('41', '41'),
    ('42', '42'),
    ('43', '43'),
    ('44', '44'),
    ('45', '45'),
    ('46', '46'),
    ('47', '47'),
    ('48', '48'),
    ('49', '49'),
    ('50', '50'),
    ('51', '51')
}

BAG_MODEL = {
    ('0', 'Baú'),
    ('1', 'Carteira/Cluth'),
    ('2', 'Carteiro'),
    ('3', 'Sacola'),
    ('4', 'Transversal'),
    ('5', 'Carteira'),
    ('6', 'Outros')
}


class Color(models.Model):
    class Meta:
        verbose_name = 'Cor'
        verbose_name_plural = 'Cores'

    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='uploads/colors')


class Texture(models.Model):
    class Meta:
        verbose_name = 'Textura'
        verbose_name_plural = 'Texturas'

    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='uploads/textures')


class Photo(models.Model):
    class Meta:
        verbose_name = 'Photo'
        verbose_name_plural = 'Photos'

    name = models.CharField(max_length=50, null=True, blank=True)
    image = models.ImageField(upload_to='uploads/photos')


class Parts(models.Model):
    class Meta:
        abstract = True
        verbose_name = 'Peça'
        verbose_name_plural = 'Peças'

    name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    mark = models.CharField(max_length=50)
    colors = models.ManyToManyField(Color, null=True, blank=True)
    textures = models.ManyToManyField(Texture, null=True, blank=True)
    images = models.ManyToManyField(Photo, null=True, blank=True)
    status = models.CharField(max_length=2, choices=PART_STATUS)


class Dress(Parts):
    class Meta:
        verbose_name = 'Vestido'
        verbose_name_plural = 'Vestidos'

    type = models.CharField(max_length=2, choices=DRESS_TYPE)
    model = models.CharField(max_length=2, choices=DRESS_MODEL)
    size = models.CharField(max_length=5, choices=SIZE)


class Blazer(Parts):
    class Meta:
        verbose_name = 'Blazer'
        verbose_name_plural = 'Blazers'

    size = models.CharField(max_length=5, choices=SIZE)


class Suit(Parts):
    class Meta:
        verbose_name = 'Paletó'
        verbose_name_plural = 'Paletós'

    size = models.CharField(max_length=5, choices=SIZE)


class Tie(Parts):
    class Meta:
        verbose_name = 'Gravata'
        verbose_name_plural = 'Gravatas'

    size = models.CharField(max_length=5, choices=TIE_SIZE)


class FormalShirt(Parts):
    class Meta:
        verbose_name = 'Camisa Social'
        verbose_name_plural = 'Camisas Social'

    arm = models.CharField(max_length=5, choices=TIE_SIZE)
    size = models.CharField(max_length=5, choices=SIZE)


class FormalPants(Parts):
    class Meta:
        verbose_name = 'Calça Social'
        verbose_name_plural = 'Calças Social'

    size = models.CharField(max_length=5, choices=PANTS_SIZE)


class Accessories(Parts):
    class Meta:
        verbose_name = 'Acessório'
        verbose_name_plural = 'Acessórios'

    type = models.CharField(max_length=5, choices=ACCESSORIES_TYPE)
    model = models.CharField(max_length=5, choices=ACCESSORIES_MODEL)
    material = models.CharField(max_length=5, choices=ACCESSORIES_MATERIAL)


class Shoe(Parts):
    class Meta:
        verbose_name = 'Sapato'
        verbose_name_plural = 'Sapatos'

    gender = models.CharField(max_length=5, choices=GENDER)
    model = models.CharField(max_length=5, choices=SHOES_MODEL)
    size = models.CharField(max_length=5, choices=SHOES_SIZE)


class Bag(Parts):
    class Meta:
        verbose_name = 'Bolsa'
        verbose_name_plural = 'Bolsas'

    gender = models.CharField(max_length=5, choices=GENDER)
    model = models.CharField(max_length=5, choices=BAG_MODEL)