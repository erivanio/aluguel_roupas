# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('email', models.EmailField(unique=True, max_length=255, verbose_name=b'Email', db_index=True)),
                ('username', models.CharField(unique=True, max_length=50)),
                ('is_active', models.BooleanField(default=True, verbose_name=b'Ativo?')),
                ('is_member', models.BooleanField(default=True, verbose_name=b'Membro?')),
                ('is_superuser', models.BooleanField(default=False, verbose_name=b'Administrador?')),
                ('created_date', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Criado em')),
                ('photo', models.ImageField(upload_to=b'uploads/user/', null=True, verbose_name=b'Foto', blank=True)),
                ('description', models.CharField(help_text=b'Deve conter no m\xc3\xa1ximo 140 caracteres', unique=True, max_length=140, verbose_name=b'Descri\xc3\xa7\xc3\xa3o')),
            ],
            options={
                'verbose_name': 'Usu\xe1rio',
                'verbose_name_plural': 'Usu\xe1rios',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Enterprise',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name=b'Nome')),
                ('logo', models.ImageField(upload_to=b'uploads/logo')),
                ('address', models.CharField(max_length=200, null=True, verbose_name=b'Endere\xc3\xa7o', blank=True)),
            ],
            options={
                'verbose_name': 'Empresa',
                'verbose_name_plural': 'Empresas',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='user',
            name='enterprise',
            field=models.ForeignKey(blank=True, to='core.Enterprise', null=True),
            preserve_default=True,
        ),
    ]
