from django.contrib import admin
from apps.core.models import User, Enterprise

admin.site.register(User)
admin.site.register(Enterprise)
