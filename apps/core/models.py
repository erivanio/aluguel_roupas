# -*- coding: utf-8 -*-
from datetime import datetime
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from django.db import models


class UserManager(BaseUserManager):
    def create_user(self, email, username, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(email=self.normalize_email(email), username=username)
        user.set_password(password)
        user.save(using=self._db)
        return user


    def create_superuser(self, email, username, password):
        user = self.create_user(username, email, password=password)
        user.is_superuser = True
        user.save(using=self._db)
        return user

class Enterprise(models.Model):
    class Meta:
        verbose_name = 'Empresa'
        verbose_name_plural = 'Empresas'

    name = models.CharField('Nome',max_length=50)
    logo = models.ImageField(upload_to='uploads/logo')
    address = models.CharField('Endereço', max_length=200, null=True, blank=True)

class User(AbstractBaseUser):
    class Meta:
        verbose_name = 'Usuário'
        verbose_name_plural = 'Usuários'

    email = models.EmailField(verbose_name='Email', max_length=255, unique=True, db_index=True)
    username = models.CharField(max_length=50, unique=True)
    is_active = models.BooleanField('Ativo?', default=True)
    is_member = models.BooleanField('Membro?', default=True)
    is_superuser = models.BooleanField('Administrador?', default=False)
    created_date = models.DateTimeField('Criado em', default=datetime.now)
    photo = models.ImageField('Foto', upload_to='uploads/user/', blank=True, null=True)
    description = models.CharField('Descrição', max_length=140, unique=True, help_text='Deve conter no máximo 140 caracteres')
    objects = UserManager()
    enterprise = models.ForeignKey(Enterprise, null=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def __unicode__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_superuser

